import Vue from 'vue'
import Vuex from 'vuex'
import attackmodule from './modules/attackmodule.js';
import levelmodule from './modules/levelmodule.js'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    attackmodule,
    levelmodule
  }
})
