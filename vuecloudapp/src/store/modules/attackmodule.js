import axios from 'axios'

const state = {
    attackdata:[1,2,3]
};

const getters = {
    getAllAttacks:(state)=>state.attackdata
};

const actions = {
    async fetchAttacks({commit}){
        const response = await axios.get('https://telemetaryproject.wm.r.appspot.com/get');
        //console.log(response.data['allattacks']);
        let CorrectedData = []
        let receivedData = response.data['allattacks'];
        for (var data in receivedData) {
            let temp = Object.values(receivedData[data]);
            console.log(temp);
            let pushData = {x: temp[0],y: temp[1]}
            CorrectedData.push(pushData);
        }
        let finaldata = [{
            name:'Damage',
            data:CorrectedData
        }]
        console.log(finaldata);     
        commit('setAttacks',finaldata);
        return(response.data['allattacks']);
    },
    addAttacks(attack){
        console.log(attack);
        axios.post('https://telemetaryproject.wm.r.appspot.com/save',attack);
    }
};

const mutations = {
    setAttacks:(state,attackdata)=>(state.attackdata = attackdata)
};

export default{
    state,
    getters,
    actions,
    mutations
}