import axios from 'axios'

const state = {
    levelpositions: [1, 2, 3],
    bubblepositions: [{
        name: 'Position',
        data: [[20, 10, 2], [20, 15, 2], [50, 15, 2]]
    }, {
        name: 'Position',
        data: [[20, 30, 2], [20, 15, 2], [50, 15, 10]]
    }]
};

const getters = {
    getallpositions: (state) => state.levelpositions,
    getbubblepositions: (state) => state.bubblepositions
};

const actions = {
    async fetchPositions({ commit }) {
        const response = await axios.get('https://telemetaryproject.wm.r.appspot.com/getpos');
        let UnCorrectedData = response.data['allpositions'];
        let CorrectedData = [];
        for (var data in UnCorrectedData) {
            let temp = Object.values(UnCorrectedData[data]);
            temp.push(10);
            CorrectedData.push(temp);
        }
       // CorrectedData.push([50,50,5]);
        let finaldata = [{
            name:'Y',
            data:CorrectedData
        }]
        commit('setBubblePositions', finaldata);
        return (CorrectedData);
    },
};

const mutations = {
    setPositions: (state, positions) => (state.levelpositions = positions),
    setBubblePositions:(state,positions)=>(state.bubblepositions = positions)
};

export default {
    state,
    getters,
    actions,
    mutations
}