import Vue from 'vue'
import VueRouter from 'vue-router'
import Admin from '../views/Admin.vue'
import AttackData from '../components/AttackData.vue'
import Site from '../views/Site.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Admin',
    component: Admin
  },
  {
    path: '/attack',
    name: 'Attack',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: AttackData
  },
  {
    path: '/site',
    name: 'Site',
    component: Site
  }
]

const router = new VueRouter({
  routes
})

export default router
