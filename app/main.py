# copyright Bishwajit Mukherjee

from flask import Flask, request, render_template, jsonify, json
from google.cloud import datastore
from flask_cors import CORS, cross_origin

app = Flask(__name__, template_folder='dist')

CORS(app)

app.config['CORS_HEADERS'] = 'Content-Type'

datastore_client = datastore.Client()


@app.route('/')
def send():
    return render_template('index.html')


@app.route('/save', methods=['GET', 'POST', 'OPTIONS'])
def save():
    if request.method == 'POST':
        attackdata = request.get_json()
        attack = attackdata['attack']
        damage = int(attackdata['damage'])
        kind = 'AttackData'
        key = datastore_client.key(kind)
        data = datastore.Entity(key=key)
        data.update(
            {
                'attack': attack,
                'damage': damage
            }
        )
        datastore_client.put(data)
        return 'working'
    return render_template('index.html')




@app.route('/get', methods=['GET', 'POST', 'OPTIONS'])
def get():
    query = datastore_client.query(kind='AttackData')
    result = list(query.fetch())
    attacks = {
        'allattacks': result
    }
    return attacks

@app.route('/getcombatdata', methods=['GET', 'POST', 'OPTIONS'])
def getcombatdata():
    query = datastore_client.query(kind='Kotlin Combat Data')
    result = list(query.fetch())
    combatdata = {
        'allcombatdata': result
    }
    return combatdata


@app.route('/getpos', methods=['GET'])
def getpos():
    query = datastore_client.query(kind='PositionData')
    result = list(query.fetch())
    positions = {
        'allpositions': result
    }
    return positions

@app.route('/getversion', methods=['GET'])
def getversion():
    query = datastore_client.query(kind='LauncherVersion')
    result = list(query.fetch())
    version_number = json.dumps(result)
    return version_number

@app.route('/savepos', methods=['GET', 'POST', 'OPTIONS'])
def savepos():
    if request.method == 'POST':
        mydata = request.get_json()
        X = int(mydata['X'])
        Y = int(mydata['Y'])
        kind = 'PositionData'
        key = datastore_client.key(kind)
        data = datastore.Entity(key=key)
        data.update(
            {
                'X': X,
                'Y': Y
            }
        )
        datastore_client.put(data)
        return mydata
    return render_template('index.html')

if __name__ == "__main__":
    print("app is running")
    app.run()
